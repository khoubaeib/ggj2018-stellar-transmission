﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public AudioSource music;                   // assign via inspector
    public AudioSource menuMusic;              // assign via inspector

    private float musicVolumeOrigin;
    private float menuMusicVolumeOrigin;

    private static SoundManager instance = null;
    public static SoundManager Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        musicVolumeOrigin = music.volume;                // makes sure transitions respect the volume from inspector
        menuMusicVolumeOrigin = menuMusic.volume;       // makes sure transitions respect the volume from inspector

        DontDestroyOnLoad(this.gameObject);
    }        

    void Start()
    {
        music.Pause();
    }

    IEnumerator MusicCrossfade(bool toGameplay, float duration)
    {
        AudioSource from = toGameplay ? menuMusic : music;
        AudioSource to = toGameplay ? music : menuMusic;
        float fromOrigin = toGameplay ? menuMusicVolumeOrigin : musicVolumeOrigin;
        float toOrigin = toGameplay ? musicVolumeOrigin : menuMusicVolumeOrigin;

        float startTime = Time.unscaledTime;
        float elapsed;

        to.UnPause();

        while ((Time.unscaledTime - startTime) < duration)
        {
            elapsed = (Time.unscaledTime - startTime) / duration;
            from.volume = Mathf.Lerp(fromOrigin, 0f, elapsed);
            to.volume = Mathf.Lerp(0f, toOrigin, elapsed);
            yield return null;
        }
        from.Pause();

        yield return null;
    }

    public void GamePlayMusic(bool restart)
    {
        if (restart)
        {
            music.Play();
        }
        StopCoroutine(MusicCrossfade(true, 0.5f));
        StartCoroutine(MusicCrossfade(true, 0.5f));
    }

    public void MenuMusic()
    {
        StopCoroutine(MusicCrossfade(false, 0.5f));
        StartCoroutine(MusicCrossfade(false, 0.5f));
    }
}
