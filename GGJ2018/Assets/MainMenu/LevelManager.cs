﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour {
    public int currentSceneIndex;
    SoundManager sound;

    // Use this for initialization
    void Start () {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentSceneIndex);
        sound = GetComponent<SoundManager>();
        Cursor.visible = true;
    }

    public void LoadLevel(string name) {
        Debug.Log("Level load requested for: " + name);
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(name);
    }
    public void LoadLevel(int number) {
        Debug.Log("Level load requested for: " + number);
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(number);

    }

    public void QuitRequest() {
        Debug.Log("I want to quit!");
        Application.Quit();
        
    }

    public void LoadNextLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

}
