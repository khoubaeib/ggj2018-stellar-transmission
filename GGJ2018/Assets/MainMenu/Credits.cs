﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {
    public GameObject credits, startButton, creditButton, backButton, quitButton;
    public SoundManager sound;

    private void Awake()
    {
        credits.SetActive(false);
        startButton.SetActive(true);
        creditButton.SetActive(true);
        backButton.SetActive(false);
        quitButton.SetActive(true);
    }

    public void OpenCredits()
    {
        credits.SetActive(true);
        startButton.SetActive(false);
        creditButton.SetActive(false);
        backButton.SetActive(true);
        quitButton.SetActive(false);
        sound.GamePlayMusic(false);
    }

    public void CloseCredits()
    {
        credits.SetActive(false);
        startButton.SetActive(true);
        creditButton.SetActive(true);
        backButton.SetActive(false);
        quitButton.SetActive(true);
        sound.MenuMusic();
    }
}
