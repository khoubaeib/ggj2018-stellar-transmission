﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earth : MonoBehaviour
{
    [SerializeField] private float SignalTime = 2;
    [SerializeField] private float GoodDistance = 2;
    [SerializeField] private float BadDistance = 10;
    
    [HideInInspector] public float SignalStrenth = 0;

    private float receptionTime = 0f;
    //private bool isReceiving = false;
    private List<Transform> linkPoints = new List<Transform>();


    void Start ()
    {
		
	}
	
	void Update ()
    {
        /*if (!isReceiving)
            return;
        
        receptionTime += Time.deltaTime;
        if (receptionTime < SignalTime)
            return;*/

        
	}


    public void LinkSatellite(LaserSource point)
    {
        receptionTime = 0f;

        if (!linkPoints.Contains(point.transform))
            linkPoints.Add(point.transform);
    }

    public void UnLinkSatellite(LaserSource point)
    {
        if (linkPoints.Contains(point.transform))
            linkPoints.Remove(point.transform);
    }


    public bool CheckConnection()
    {
        if (linkPoints.Count == 0)
            return false;
        
        bool check = false;
        foreach(var l in linkPoints)
            check = check || l.GetComponent<LaserSource>().CheckPreviousConnection(new List<Transform>());

        return check;
    }
    

    public void StartReceive()
    {
        /*if (isReceiving)
            return;

        isReceiving = true;
        receptionTime = 0f;*/


        SignalReceived();
    }

    public void StopReceive()
    {
        /*if (!isReceiving)
            return;

        isReceiving = false;
        linkPoint = null;*/

    }

    public void SignalReceived()
    {
        List<float> distances = new List<float>();
        float bestDistance = 0;

        foreach (var l in linkPoints)
        {
            if(l.GetComponent<LaserSource>().CheckPreviousConnection(new List<Transform>()))
            {
                distances.Add(Vector3.Distance(l.position, transform.position));
            }
        }

        if(distances.Count == 0)
        { 
            GameManager.Instance.UpdateSignalStrenth(0);
            return;
        }

        bestDistance = distances[0];
        for(int i = 0; i < distances.Count; i++)
        {
            if (bestDistance > distances[i])
                bestDistance = distances[i];
        }
        
        GameManager.Instance.UpdateSignalStrenth(1 - Mathf.InverseLerp(GoodDistance, BadDistance, bestDistance));
    }
}
