﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WordStorage : MonoBehaviour {

    public string[] funnyWords = new string[] { "CATS", "GAMEJAM", "BALLS", "SKELLEFTEA", "FUNNY", "WORD", "BIRD", "PIZZA", "ARCTIC", "HOMANS", "GUINEAPIG", "SPACE", "SATELLITE", "TRANSMISSION", "ASTEROID", "PLANET", "EARTH", "ALIEN", "PEACE", "FRIENDSHIP", "LOVE", "FLOWERS", "FINGERS", "WIFI", "SELFIE", "WALL", "SANTA", "SODA", "GLADOS", "PROBLEM", "WAR", "XOXO", "LOL", "ROFL", "SMH", "DEATHSTAR", "SNAPEKILLSDUMBLEDORE", "HELLOWORLD", "LMAO", "THEGREATNORTHERN" };
    public Text secretWord;
    public float signalStrength;
    private int howManyLettersToShow;
    private int letterRevealedMode = 0;
    string randomWord;

    // Use this for initialization
    void Start () {
        randomWord = funnyWords[Random.Range(0, funnyWords.Length)];
        Debug.Log(randomWord);
        Debug.Log(randomWord.Length);

        for (int i = 0; i < randomWord.Length; i++) {
            secretWord.text += "?";
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (signalStrength < 21 && letterRevealedMode != 0) {
            secretWord.text = "";
            for (int i = 0; i < randomWord.Length; i++) {
                secretWord.text += "?";
            }
            letterRevealedMode = 0;
        } else if (signalStrength >= 21 && signalStrength < 41 && letterRevealedMode != 1) {
            revealLetters();
            letterRevealedMode = 1;
        } else if (signalStrength >= 41 && signalStrength < 61 && letterRevealedMode != 2) {
            revealLetters();
            letterRevealedMode = 2;
        } else if (signalStrength >= 61 && signalStrength < 81 && letterRevealedMode != 3) {
            revealLetters();
            letterRevealedMode = 3;
        } else if (signalStrength >= 81) {
            revealLetters();
            letterRevealedMode = 3;
        }

    }
    void revealLetters() {
        howManyLettersToShow = (int)((float)randomWord.Length * (signalStrength / 100));
        Debug.Log(howManyLettersToShow);
        secretWord.text = "";
        for (int i = 0; i < howManyLettersToShow; i++) {
            secretWord.text = secretWord.text.Insert(i, "" + randomWord[i]);
            //secretWord.text[i] = randomWord[i];
        }

        while (secretWord.text.Length < randomWord.Length) {
            secretWord.text += "?";
        }
    }
}
