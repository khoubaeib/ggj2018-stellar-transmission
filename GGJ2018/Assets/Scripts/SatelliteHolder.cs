﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteHolder : MonoBehaviour
{

    [SerializeField] private Transform Satellite;
    [SerializeField] private float distance;

    private bool selected = false;
    private Vector2 previous;

    void Start ()
    {
        enabled = Satellite != null;
        previous = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
	
	void Update ()
    {
        if (!selected)
            return;

        previous = Camera.main.ScreenToWorldPoint(Input.mousePosition); //Vector2.Lerp(previous, Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.05f);
        Vector2 direction = previous - (Vector2)transform.position;
        Satellite.position = (Vector2)transform.position + (direction.normalized * distance); 
	}

    private void OnMouseDown()
    {
        selected = true;
    }
    

    private void OnMouseUp()
    {
        selected = false;
    }
    

}
