﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    public LevelManager levelManager;
    public Text counter;

    public float timeLeft = 30.0f;

	// Use this for initialization
	void Start () {


    }
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        counter.text = "" + (int)timeLeft;
        if (timeLeft < 0) {
            levelManager.LoadLevel("MainMenu");
        }



    }
}
