﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public string text;
    public static GameManager Instance = null;

    [HideInInspector] public string[] funnyWords = new string[] { "CATS", "GAMEJAM", "BALLS", "SKELLEFTEA", "FUNNY", "WORD", "BIRD", "PIZZA", "ARCTIC", "HOMANS", "GUINEAPIG", "SPACE", "SATELLITE", "TRANSMISSION", "ASTEROID", "PLANET", "EARTH", "ALIEN", "PEACE", "FRIENDSHIP", "LOVE", "FLOWERS", "FINGERS", "WIFI", "SELFIE", "WALL", "SANTA", "SODA", "GLADOS", "PROBLEM", "WAR", "XOXO", "LOL", "ROFL", "SMH", "DEATHSTAR", "SNAPEKILLSDUMBLEDORE", "HELLOWORLD", "LMAO", "THEGREATNORTHERN" };
    [HideInInspector] public float SignalStrenth = 0;

    public float GuessDelay = 0f;
    public GameObject Earth;
    public GameObject Alien;

    public Text timerField;
    public Text scoreField;
    public Text hintWord;
    public InputField guessedWord;

    public LevelManager levelManager;

    private float timer = 0;
    private string correctWord;

    void Start ()
    {
        Instance = this;
        timer = GuessDelay;

        timerField.text = "Time : " + (int)GuessDelay;
        scoreField.text = "Strenth :  0%";

        correctWord = funnyWords[UnityEngine.Random.Range(0, funnyWords.Length)];
    }

    void Update()
    {
        GuessDelay -= Time.deltaTime;
        timerField.text = "Time : " + (int)GuessDelay;

        if (GuessDelay <= 0)
            GameOver();
    }


    public void CheckEarthConnection()
    {
        if (Earth.GetComponent<Earth>().CheckConnection())
            Earth.GetComponent<Earth>().StartReceive();
        else
            Earth.GetComponent<Earth>().StopReceive();

    }

    public void UpdateSignalStrenth(float strenth)
    {
        SignalStrenth = strenth;
        scoreField.text = "Strenth : " + (int)(SignalStrenth * 100) + "%";
        hintWord.text = EncryptWord(correctWord, SignalStrenth);

        if (!hintWord.text.Contains("?"))
            guessedWord.text = hintWord.text;

    }

    private string EncryptWord(string correctWord, float signalStrenth)
    {
        String encrypted = "";

        for (int i = 0; i < correctWord.Length; i++)
            encrypted += "?";

        int howManyLettersToShow = (int)((float)correctWord.Length * signalStrenth);

        char[] charTable = encrypted.ToCharArray();

        for (int i = 0; i < howManyLettersToShow; i++)
            charTable[i] = correctWord[i];

        encrypted = "";

        for (int i = 0; i < charTable.Length; i++)
            encrypted += charTable[i];

        return encrypted;
    }

    public void CheckInput()
    {
        if (correctWord.Equals(guessedWord.text.ToUpper()))
            GameWon();
    }

    private void GameWon()
    {
        Debug.Log("Game won -> pass to next level !");
        levelManager.LoadLevel(text);
    }

    private void GameOver()
    {
        Debug.Log("Planet will be destroyed !");
        levelManager.LoadLevel("GameOver");

    }

}
