﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSource : MonoBehaviour
{
    public bool IsPrimarySource = false;
    public float MaxDistance = 5;
    //public LayerMask SatelliteLayer;

    [HideInInspector] public bool IsConnected = false;
    [HideInInspector] public Transform ConnectionSource;

    private LineRenderer laser;
    private Transform target;
    

    void Start ()
    {
        CheckLaserLine();
    }

    void LateUpdate()
    {
        RaycastHit2D[] hitPoints = new RaycastHit2D[1];

        int count = GetComponent<Collider2D>().Raycast(-transform.up, hitPoints, MaxDistance);        
        Debug.DrawRay(transform.position, -transform.up);

        Vector3 targetPoint;
        Color color = Color.white;

        if (count > 0 && (hitPoints[0].transform.CompareTag("Satelite") || hitPoints[0].transform.CompareTag("Earth")))
        {
            targetPoint = hitPoints[0].point;
            CheckLink(hitPoints[0].transform);
        }
        else
        {
            targetPoint = transform.position;
            StopPreviousTarget();
        }

        DrawRayCast(targetPoint, color);
    }


    public bool CheckPreviousConnection(List<Transform> list)
    {
        if (IsPrimarySource)
            return true;

        foreach (var l in list)
            if (l == transform) return false;

        list.Add(transform);
        return IsConnected && ConnectionSource.GetComponent<LaserSource>().CheckPreviousConnection(list);
    }

    void CheckLaserLine()
    {
        if (laser)
            return;

        laser = GetComponent<LineRenderer>();
        if (!laser)
            laser = transform.gameObject.AddComponent<LineRenderer>();
    }

    void DrawRayCast(Vector3 endPoint, Color color)
    {
        laser.startColor = color;
        laser.SetPosition(0, transform.position);
        laser.endColor = color;
        laser.SetPosition(1, endPoint);

        Debug.DrawLine(transform.position, endPoint);
    }

    public void CheckLink(Transform pointTo)
    {
        if (target == pointTo)
            return;

        StopPreviousTarget();
        if (!CheckNewTarget(pointTo))
            return;
        
        target = pointTo;

        if (target.GetComponent<LaserSource>())
            target.GetComponent<LaserSource>().EstablishConnection(transform);

        if (target.GetComponent<Earth>())
            target.GetComponent<Earth>().LinkSatellite(this);

        GameManager.Instance.CheckEarthConnection();
    }

    private void StopPreviousTarget()
    {
        if (!target)
            return;
        
        var earth = target.GetComponent<Earth>();
        if (earth)
            earth.StopReceive();

        var source = target.GetComponent<LaserSource>();
        if (source)
            source.ShutDownConnection();

        target = null;

        GameManager.Instance.CheckEarthConnection();
    }

    private bool CheckNewTarget(Transform pointTo)
    {
        return pointTo.GetComponent<Earth>() || (pointTo.GetComponent<LaserSource>() && !pointTo.GetComponent<LaserSource>().IsPrimarySource);
    }

    public void EstablishConnection(Transform source)
    {
        IsConnected = true;
        ConnectionSource = source;
    }

    public void ShutDownConnection()
    {
        IsConnected = false;
        ConnectionSource = null;
    }
}
