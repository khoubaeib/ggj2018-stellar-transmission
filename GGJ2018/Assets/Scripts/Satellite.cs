﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satellite : MonoBehaviour {

    private bool selected = false;
    

    void Update()
    {
        if (!selected)
            return;

        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        
        this.transform.eulerAngles = 
            new Vector3(0, 0, Mathf.Atan2((mouse.y - transform.position.y), (mouse.x - transform.position.x)) * Mathf.Rad2Deg - 90);
    }

    private void OnMouseDown()
    {
        selected = true;
    }


    private void OnMouseUp()
    {
        selected = false;
    }
}
