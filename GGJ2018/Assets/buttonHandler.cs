﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonHandler : MonoBehaviour {

    LevelManager manage;

	// Use this for initialization
	void Start () {
        manage = GameObject.FindGameObjectWithTag("LevelManager").gameObject.GetComponent<LevelManager>();
        Debug.Log(manage.currentSceneIndex);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Continue() {
        manage.LoadNextLevel();
    }
    public void Restart() {
        manage.RestartLevel();
    }
}
