﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    public GameObject planetPos;

    public float rotationSpeed;
    public bool rotateX, rotateY, rotateZ;
    public bool positiveRotation = true;
    float rotationX, rotationY, rotationZ;

    private void Start()
    {
        if (planetPos != null)
            gameObject.transform.position = planetPos.transform.position;
    }

    // Update is called once per frame
    void Update () {
        if (rotateX) rotationX = rotationSpeed * Time.deltaTime;
        else rotationX = 0;
        if (rotateY) rotationY = rotationSpeed * Time.deltaTime;
        else rotationY = 0;
        if (rotateZ) rotationZ = rotationSpeed * Time.deltaTime;
        else rotationZ = 0;
        gameObject.transform.Rotate(rotationX, rotationY, rotationZ);
    }
}
